var Promise = require('es6-promise').Promise;

var webpack = require("webpack");
      module.exports = {
          entry: './app/app.js',
          output: {
            filename: 'bundle.js',
            path: __dirname + '/public'
          },
          watch: true,
          module: {
              loaders: [
                  {
                      test: /\.css$/,
                      loader: "style!css"
                  },
                  {
                    test: /\.scss$/,
                    loader: "style!css!sass"
                  },
                  {
                    test: /\.jsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "babel",
                    query: {
                      presets: ["react", "es2015","stage-2"]
                    }
                  },
                  {
                    test: /\.(woff|woff2)$/,
                    loader: "url-loader?limit=10000&mimetype=application/font-woff"
                  },
                  {
                    test: /\.ttf$/,
                    loader: "file-loader"
                  },
                  {
                    test: /\.eot$/,
                    loader: "file-loader"
                  },
                  {
                    test: /\.svg$/,
                    loader: "file-loader"
                  },
                  { test: /\.jpe?g$|\.gif$|\.png$/i, loader: "file" },
              ]
          },
          plugins: [
              new webpack.ProvidePlugin({
                  $: "jquery",
                  jQuery: "jquery",
                  "window.jQuery": "jquery"
              })
          ]
      };
