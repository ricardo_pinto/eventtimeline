var React = require('react');

var Summary = React.createClass({


    showSummary: function() {
        if(this.props.data == null)
            return null;
        //var textList = [];
        var imgRender = {
            "baseline1":[],
            "baseline2":[],
            "baseline3":[],
            "baseline4":[],
        };
        //var summary_type = this.props.data["summary_type"];
        var episode_num = this.props.data["episode_num"];
        var summary_fixed = this.props.data["summary_fixed"];
        var summary_dynamic = this.props.data["summary_dynamic"];
        var border_color = this.props.data["border_color"];


        //console.log("border_color: "+border_color);
        /*if(summary_type == 1){
            for (var i = 0; i < this.props.data["texts"].length; i++) {

                var summ_text = this.props.data["texts"][i][1];
              textList[i] = (
                <div key={"summtxt_"+i} className="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
                  <div className="post_item">
                    <div className="post_info">
                      <p>{summ_text}</p>
                    </div>
                  </div>
                </div>
              );
            }
        }*/



        for (var i = 0; i < summary_fixed.length; i++) {

            var summ_img = summary_fixed[i];
          imgRender["baseline1"][i] = (
            <div key={"summimg_b1_"+i} className="pure-u-1-5">
                <div className="summ_image">
                  <img src={"/data/"+summ_img} className="pure-img"/>
                </div>
            </div>
          );
        }


        if(summary_dynamic == null){
            return(
                <div className=" panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingOne">
                            <h3 role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Images selected to summarize the event: <small>(click to show/hide)</small>
                            </h3>
                        </div>
                        <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div className="panel-body ">
                                <div className="pure-g">
                                    {imgRender["baseline1"]}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            var rel_terms=[];
            var summTerms = this.props.data["summary_terms"][episode_num-1];
            var mostRelevantTerms = summTerms.slice(0,4);
            var sortedTerms = summTerms.sort();
            for(var i = 0; i < sortedTerms.length;i++) {
                var sep = " ";
                if(mostRelevantTerms.includes(sortedTerms[i]))
                    rel_terms[i] = (<span key={"rel_terms_"+i} style={{fontSize:"1.6em"}}>{sortedTerms[i]}{sep}</span>);
                else {
                    rel_terms[i] = (<span key={"rel_terms_"+i}>{sortedTerms[i]}{sep}</span>);
                }

            }
            for (var j = 0; j < Object.keys(summary_dynamic).length;j++){
                var blnum = j+2;
                var baseline_str = "baseline"+blnum;
                var key_str = "summimg_b"+blnum+"_";
                for (var i = 0; i < summary_dynamic[baseline_str].length; i++) {

                    var summ_img = summary_dynamic[baseline_str][i];
                    //console.log("summ_img:"+summ_img);
                    imgRender[baseline_str][i] = (
                    <div key={{key_str}+i} className="pure-u-1-5">
                        <div className="summ_image">
                          <img src={"/data/"+summ_img} className="pure-img"/>
                        </div>
                    </div>
                  );
                }
            }

            return(
                <div className=" panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4>Most relevant terms to <span style={{color:border_color }}>episode {episode_num}</span>: </h4>
                        </div>
                        <div className="panel-body">
                            <p>{rel_terms}</p>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingOne">
                            <h3 role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Images selected to summarize the event (Baseline 1): <small>(click to show/hide)</small>
                            </h3>
                        </div>
                    </div>
                    <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div className="panel-body ">
                        <div className="pure-g">
                            {imgRender["baseline1"]}
                        </div>
                    </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingTwo">
                            <h3 role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                Images selected to summarize <span style={{color:border_color }}>episode {episode_num}</span> (Baseline 2): <small>(click to show/hide)</small>
                            </h3>
                        </div>
                    </div>
                    <div id="collapseTwo" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div className="panel-body ">
                        <div className="pure-g">
                            {imgRender["baseline2"]}
                        </div>
                    </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingThree">
                            <h3 role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                Images selected to summarize <span style={{color:border_color }}>episode {episode_num}</span> (Baseline 3): <small>(click to show/hide)</small>
                            </h3>
                        </div>
                    </div>
                    <div id="collapseThree" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                    <div className="panel-body ">
                        <div className="pure-g">
                            {imgRender["baseline3"]}
                        </div>
                    </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingFour">
                            <h3 role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                Images selected to summarize <span style={{color:border_color }}>episode {episode_num}</span> (Baseline 4): <small>(click to show/hide)</small>
                            </h3>
                        </div>
                    </div>
                    <div id="collapseFour" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                    <div className="panel-body ">
                        <div className="pure-g">
                            {imgRender["baseline4"]}
                        </div>
                    </div>
                    </div>
                </div>
            );
        }
    },
    render: function() {
        return (this.showSummary());
    }
});

module.exports = Summary;
