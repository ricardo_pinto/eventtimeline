var React = require('react');

var TimeLine = React.createClass({
    getInitialState: function(){
		return{
			docs: null,
            timeIndex: null,
		}
	},
    componentDidUpdate: function(){
        if(this.state.timeIndex != this.props.timeline){
            this.setState({
                timeIndex:this.props.timeline,
                docs: null,
            });
        }
      if(this.props.timeline != null && this.state.docs == null){
  		var self = this;
  		$.get('/data/gallery_data.json',function(data){
              self.setState({
      			docs:data[self.props.timeline.toString()],
      		});
  		});
      }
    },
  buildInfo: function(){
    if (this.props.timeline == null) {
        return null;
    }
    if(this.state.docs == null){
        console.log("null");
      return null;
    }else{
      var postsList = [];

      for (var i = 0; i < this.state.docs.length; i++) {
          var img_tag = null;
          if (this.state.docs[i][2] != null){
              var image = "/data/"+this.state.docs[i][2];
              img_tag = (<div className="post_image">
                <img src={image}/>
              </div>);
          }

          var doc_text = this.state.docs[i][1];
        postsList[i] = (
          <div key={"post_"+i} className="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
            <div className="post_item">
              {img_tag}
              <div className="post_info">
                <p>{doc_text}</p>
              </div>
            </div>
          </div>
        );
      }

      return(
        <div className="timeline_wrapper">
          <p className="title">Documents for day: {this.props.timeline}</p>
          <div className="posts_list row">
            {postsList}
          </div>
        </div>
      );
    }

  },

	render: function() {
    return(
      this.buildInfo()
    );

	}
});

module.exports = TimeLine;
