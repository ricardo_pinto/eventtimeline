var React = require('react');

var Main = require('./Main.js');
var TopicList = React.createClass({

    getInitialState: function(){
        return {
            topics: null
        };
    },

    topicClick: function(topic_num){
        console.log("topicClick");
        console.log("topic "+topic_num);
        this.props.changePage(topic_num);
    },


    buildTopicList: function(){
        console.log(this.props.data);
        var topics = this.props.data;
        if(topics != null){
            var topiclist = [];

            for (var topic_num in topics){

                topiclist.push((<button type="button" key={topic_num} onClick={this.topicClick.bind(this,topic_num)} className="list-group-item">{topic_num}: {topics[topic_num]}</button>));
            }

            return (
            <div>
                <h3>List of topics</h3>

                <div className="row">
                    <div className="col-xs-6">
                        <div className="list-group">
                            {topiclist.slice(0,topiclist.length/2.)}
                        </div>
                    </div>
                    <div className="col-xs-6">
                        <div className="list-group">
                            {topiclist.slice(topiclist.length/2.,topiclist.length)}
                        </div>
                    </div>
                </div>
            </div>
            );
        } else return null;
    },

    render: function() {
		return(
      		<div>

                {this.buildTopicList()}


			</div>
    	);
	}
});

module.exports = TopicList;
