var React = require('react');
//var Chart = require('chart.js');
var Highcharts = require('highcharts');
require('highcharts/modules/exporting')(Highcharts);
var myChart = null, chartConfig = null;

var Graph = React.createClass({

    getInitialState: function(){
        return {
            topic: -1,
            data: null,
        };
    },

    componentWillReceiveProps: function() {
        if(this.props.topic != null &&  this.props.topic != this.state.topic
            && this.props.data != null && this.props.data["topic_title"] != this.state.data["topic_title"]){
            this.setState({
                topic: this.props.topic,
                data: this.props.data,
            });

                console.log("componentWillReceiveProps: Graph.js");
                console.log("topic "+this.props.topic);
            myChart = null;
            this.buildGraph();
        }
    },

  componentDidUpdate: function(){
    if(this.props.data != null && myChart == null ){
        this.setState({
            data: this.props.data,
        });

        console.log("componentDidUpdate: Graph.js");
        this.buildGraph();
    }
  },

  componentDidMount: function(){
    if(this.props.data != null && myChart == null){
        this.setState({
            data: this.props.data,
        });
        console.log("componentDidMount: Graph.js");
        this.buildGraph();
    }
  },

  componentWillUnmount: function(){
    myChart = null;
  },

  buildGraph: function(){

    if(this.props.data != null){
        console.log("buildGraph "+this.props.data["topic_title"]);
        var topic_title = this.props.data["topic_title"];
        var hdata = this.props.data["histogram"];
        var hlabels = this.props.data["labels"];
        var peaks = this.props.data["peaks"];
        var ndocs = this.props.data["ndocs"];
        var nimgs = this.props.data["nimgs"];
        var subtitle = 'Presenting '+ndocs+' documents, including '+nimgs+' images';
        var self = this;  //save scope to access it inside chart config

        var seriesArray = [];
        var ep_count = 1;
        var newSeries;
        peaks.forEach(function(p){
            newSeries = [];

            //newSeries.push([p[0]-0.5,0]);
            //for(var i = 0; i < p.length; i++){
            //    newSeries.push([p[i],hdata[p[i]]]);
            //}
            //newSeries.push([p[p.length-1]+0.5,0]);
            for(var i = 0; i<hdata.length; i++){
                if(p.includes(i)){
                    newSeries.push(hdata[i]);
                } else
                    newSeries.push(0);
            }
            seriesArray.push({
                name: "Episode "+ep_count,
                data: newSeries,

            });
            ep_count++;
        });
        /*var seriesZero = [];
        for(var i = 0; i < hdata.length; i++){
            if (hdata[i] == 0){
                seriesZero.push([i,0]);
            }
        }
        if(seriesZero.length > 0){
            seriesArray.push({
                data:seriesZero,
                showInLegend: false,
                tooltip: {
                    enabled: false,
                }
            });
        }*/
        myChart = Highcharts.chart('chartWrapper', {
            chart: {

                type: 'column',
                 zoomType: 'x'
            },
            title: {
                text: topic_title
            },
            subtitle: {
            //    text: subtitle
            },
            xAxis: {
                categories: hlabels,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Document Frequency'
                }
            },
            legend: {
                enabled: false,
            },
            tooltip: {
                /*headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} </td>' +
                    '<td style="padding:0"><b></b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true*/
                headerFormat: '<b>{point.key}</b><br/>',
                pointFormat: '{series.name}'
            },
            plotOptions: {
                column: {
                    /*pointPadding: 0.05,
                    borderWidth: 0,*/
                    stacking: 'normal',
                    /*dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    } ,*/
                    events: {
                        click: function () {
                            //Graph.chartClick(this.index);
                          //self.props.handleBarClick(this.index);
                          console.log(this.name);
                          console.log(this.color);
                          var t = this.name.replace("Episode ","");
                          var peak = parseInt(t) - 1 ;
                          console.log(peak);
                          self.props.handleBarClick(peak,this.color);
                        }
                    },
                },
                /*area: {
                    fillOpacity: 0.8,
                    events: {
                        click: function () {
                            //Graph.chartClick(this.index);
                          //self.props.handleBarClick(this.index);
                          console.log(this.name);
                          var t = this.name.replace("Episode ","");
                          var peak = parseInt(t) - 1 ;
                          console.log(peak);
                          self.props.handleBarClick(peak);
                        }
                    },
                },*/
                series: {
                    marker: {
                            enabled: false,
                    },
                    //pointwidth: 20,
                }
            },
            series: seriesArray,
        });
        //console.log(myChart.series);
    }
  },

	render: function() {
		return(

      <div className="graph_wrapper" >
        <div id="chartWrapper">

        </div>
			</div>
    );
	}
});

module.exports = Graph;
