var React = require('react');

var Main = require('./Main.js');
var TopicList = require('./TopicList.js');

var Wrapper = React.createClass({

    getInitialState: function(){
        return {
            topic: -1,
            topics: null,
            hasNext: true,
            hasPrev: true,
        };
    },

    componentDidMount: function(){
		var self = this;

		$.get('/data/topic_list.json',function(data){
			self.setState({
                topics: data,
            });
		});
	},

    changePage: function(topic_num){
        console.log("changePage");
        console.log("topic " + topic_num);
        var topics = Object.keys(this.state.topics).sort();
        var prev = true;
        var next = true;
        if(topics.indexOf(topic_num) == 0){
            prev = false;
        }
        if(topics.indexOf(topic_num) == topics.length-1){
            next = false;
        }
        this.setState({
            topic: topic_num,
            hasNext: next,
            hasPrev: prev,
        });
    },

    changeNext: function() {
        var topics = Object.keys(this.state.topics).sort();
        var topic_num = topics[topics.indexOf(this.state.topic) + 1];
        var next = true;
        if(topics.indexOf(topic_num) == topics.length-1){
            next = false;
        }
        console.log("changeNext: "+topic_num);
        this.setState({
            topic: topic_num,
            hasNext: next,
            hasPrev: true,
        });
    },

    changePrevious: function() {
        var topics = Object.keys(this.state.topics).sort();
        var topic_num = topics[topics.indexOf(this.state.topic) - 1];
        var prev = true;
        if(topics.indexOf(topic_num) == 0){
            prev = false;
        }
        console.log("changePrevious: "+topic_num);
        this.setState({
            topic: topic_num,
            hasPrev: prev,
            hasNext: true,
        });
    },

    render: function() {
        if (this.state.topic < 0) {
            return (<TopicList data={this.state.topics} changePage={this.changePage}/>);
        } else {
            return (<Main topic={this.state.topic}
                hasNext={this.state.hasNext} 
                hasPrev={this.state.hasPrev}
                changePage={this.changePage}
                changePrevious={this.changePrevious}
                changeNext={this.changeNext}/>);
        }
    }
});

module.exports = Wrapper;
