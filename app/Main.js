var React = require('react');

var Graph = require('./Graph.js');
var TimeLine = require('./TimeLine.js');
var Summary = require('./Summary.js');

var Main = React.createClass({

	getInitialState: function(){
		return{
			topic: null,
			timeline: null,
			data: null,
			summary: null,
		}
	},

	componentDidMount: function(){
		if (this.props.topic != null){
			this.setState({
				topic: this.props.topic,
			});
			this.buildGraphAndSummary(this.props.topic);
		}
	},

	componentDidUpdate: function() {
		if ( this.props.topic != null && this.state.topic != this.props.topic){
			this.setState({
				topic: this.props.topic,
			});
			this.buildGraphAndSummary(this.props.topic);
		}
	},

	buildGraphAndSummary: function(topic_num) {
		console.log("buildGraphAndSummary: Main.js");
		console.log("topic_num "+topic_num);
		var self = this;
		$.get('/data/topics/'+topic_num+'/graph_data.json',function(data){

			self.buildGraphData(data);
			self.setState({
				peaks: data["peaks"]
			});
		});
		$.get('/data/topics/'+topic_num+'/summary_data.json',function(data){
			self.buildSummaryData(data);
		});
	},

	buildGraphData: function(data){
		var titleToShow = data["topic_title"];
		var histogram = data["histogram"];
		var relevant = data["relevant_periods"];
		var peaks = data["peaks"];
		var labels = data["labels"];
		var ndocs = data["total_ndocs"];
		var nimgs = data["total_nimgs"];

		//var colors = [];
		var histogramToShow = new Array(histogram.length);

		var ci = 0;
		for (var i = 0; i < histogram.length; i++) {

			if (i == relevant[ci]){
				//colors[relevant[i]] = "rgba(54, 162, 235, 0.2)";
				histogramToShow[i] = histogram[i];
				if(ci < relevant.length){
					ci++;
				}
			} else {
				histogramToShow[i] = 0;
			}
		}

		this.setState({
			data: {
				topic_title: titleToShow,
				histogram: histogramToShow,
				peaks: peaks,
				//colors: colors,
				labels: labels,
				ndocs: ndocs,
				nimgs: nimgs,
			}
		});

	},
	buildSummaryData: function(data){
		this.setState({
			summaries:{
				summary_fixed:data["summaries"]["images1"],
				summary_dynamic:null,
				images:[
					data["summaries"]["images2"],
					data["summaries"]["images3"],
					data["summaries"]["images4"],
				],
				episode_num: -1,
				summary_terms:data["relevant_terms"],
			}
		});


	},


	handleBarClick: function(index,color){
		//colors = this.state.data["colors"];
		//colors[index] = "rgba(14, 53, 78, 0.2)"
		console.log("bar clicked: "+index);
		console.log("color: "+color);
		this.setState({
			summaries:{
				episode_num: index+1,
				summary_fixed:this.state.summaries["summary_fixed"],
				summary_dynamic:{
					"baseline2":this.state.summaries["images"][0][index],
					"baseline3":this.state.summaries["images"][1][index],
					"baseline4":this.state.summaries["images"][2][index],
				},
				images:this.state.summaries["images"],
				summary_terms:this.state.summaries["summary_terms"],
				border_color:color,
			}
			/*summary:{
				summary_type:this.state.summary["summary_type"],
				episode_num: index+1,
				images: this.state.summary_images[index],
				rel_terms: this.state.summary_terms[index],
			}*/
		});
		/*
		if (this.state.summary["summary_type"]==2){
			var peaks = this.state.peaks;
			console.log("peaks length: "+peaks.length);
			for(var i=0; i< peaks.length; i++){
				console.log("peak: "+peaks[i]);
				if(peaks[i].includes(index)){
					console.log("HIT");
					this.setState({
						summary:{
							summary_type:this.state.summary["summary_type"],
							images: this.state.summary_images[i],
						}
					});
					break;
				}

			}

		}*/
		/*this.setState({
			timeline:index,
		});*/

	},

	changePrevious: function() {
		this.props.changePrevious();
	},

	changeIndex: function() {
		this.props.changePage(-1);
	},

	changeNext: function() {
		this.props.changeNext();
	},

	render: function() {
		var prevButton = (<div/>);
		var nextButton = (<div/>);
		if(this.props.hasPrev){
			prevButton = (
				<button type="button" className="btn btn-primary" onClick={this.changePrevious}>Previous Topic</button>
			);
		}
		if(this.props.hasNext){
			nextButton = (
				<button type="button" className="btn btn-primary" onClick={this.changeNext}>Next Topic</button>
			);
		}
		return(
      		<div>
				<div>
					{prevButton}
					<button type="button" className="btn btn-default" onClick={this.changeIndex}>Index</button>
					{nextButton}
				</div>
				<Graph data={this.state.data} topic={this.state.topic} handleBarClick={this.handleBarClick}/>
				<Summary data={this.state.summaries}/>

			</div>
    	);
		//<TimeLine timeline={this.state.timeline}/>
	}
});

module.exports = Main;
